package saitarly.saitarly.drivers;

import lombok.Getter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

@Getter
public class WebDriverHolder {
    private static WebDriverHolder instance = null;
    private final WebDriver driver;

    @Getter
    private final WebDriverWait wait;

    private WebDriverHolder() {
        driver = WebDriverFactory.initDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));
    }

    public synchronized static WebDriverHolder getInstance() {
        if (instance == null){
            instance = new WebDriverHolder();
        }
        return instance;
    }

    public void quitDriver(){
        if (driver != null){
            driver.quit();
        }
    }
    public static class ScrollHelper{
        public static void scrollToTop(){
            JavascriptExecutor js = (JavascriptExecutor) WebDriverHolder.getInstance().getDriver();
            js.executeScript("window.scrollTo(0, 0)");
        }
    }
}

