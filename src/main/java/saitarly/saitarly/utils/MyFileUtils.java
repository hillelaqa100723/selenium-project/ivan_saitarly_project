package saitarly.saitarly.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import static java.lang.Thread.sleep;
@Slf4j
public class MyFileUtils {
    public static File getDownloadsFolder(){
        return new File(Constants.DOWNLOAD_FOLDER);
    }

    public static void prepareDownloadsFolder() {
        log.info("Init download folder!");
        File downloadsFolder = getDownloadsFolder();
        if (!downloadsFolder.exists()) {
            downloadsFolder.mkdir();
        } else if (downloadsFolder.isDirectory()) {  // Добавил условие, чтобы не удалять каталог верхнего уровня
            try {
                FileUtils.cleanDirectory(downloadsFolder);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }


    public static File getScreenshotsFolder() {
        return new File(Constants.SCREENSHOT_FOLDER);
    }

    public static void prepareScreenshotsFolder() {
        log.info("Init screenshots folder!");
        if (!getScreenshotsFolder().exists()) {
            getScreenshotsFolder().mkdir();
        } else {
            try {
                FileUtils.cleanDirectory(getScreenshotsFolder());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }


    public static File waitTillFileIsDownloaded(File file) {
        log.info("Wait until file downloaded");
        int timeout = 60;
        int count = 0;

        while (!file.exists() && count < timeout) {
            try {
                sleep(Duration.ofSeconds(3).toMillis());
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            count += 3;
        }
        count = 0;
        while (count < timeout) {
            long lengthBefore = file.length();
            try {
                sleep(Duration.ofSeconds(3).toMillis());
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            long lenghtAfter = file.length();
            if (lengthBefore == lenghtAfter){
                return file;
            }
            count += 3;
        }
        return file;
    }

}
