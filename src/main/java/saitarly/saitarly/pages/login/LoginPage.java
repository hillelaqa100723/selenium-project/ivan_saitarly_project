package saitarly.saitarly.pages.login;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import saitarly.saitarly.pages.base.BasePage;
import saitarly.saitarly.utils.PropertyReader;

public class LoginPage extends BasePage {


    @FindBy(id = "Email")
    private static WebElement emailField;

    @FindBy(name = "Password")
    private static WebElement passwordField;

    @FindBy(css = ".login-button")
    private static WebElement loginButton;

    @FindBy(css = ".register-button")
    public WebElement registerButton;

    @FindBy(id = "RememberMe")
    private WebElement rememberMe;

    @FindBy(xpath = "//h1[contains(text(),'Welcome, Please Sign In!')]")
    private WebElement pageTitle;



    public static BasePage login(String userEmail, String userPassword){
        enterDataToInput(emailField, userEmail);
        enterDataToInput(passwordField, userPassword);
        loginButton.click();
        return new BasePage();
    }

    public LoginPage loginIncorrect(String userEmail, String userPassword){
        enterDataToInput(emailField, userEmail);
        enterDataToInput(passwordField, userPassword);
        loginButton.click();
        return new LoginPage();
    }

    private static void enterDataToInput(WebElement element, String value){
        element.clear();
        element.sendKeys(value);
    }

    public BasePage loginWithDefaultUser(){
        return login(PropertyReader.getInstance().getProperty("defaultUser"), PropertyReader.getInstance().getProperty("userPass"));
    }
    public boolean isTheLoginPageLoaded() {
        return pageTitle.isDisplayed();
    }
}
