package saitarly.saitarly.pages.DigitalDownload;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import saitarly.saitarly.drivers.WebDriverHolder;
import saitarly.saitarly.pages.base.BasePage;

@Slf4j
public class DigitalDownloads extends BasePage {

    public DigitalDownloadsProductPage selectProductItem (String itemName) {
        log.info("Selecting a product item: {}", itemName);
        WebDriverHolder.getInstance()
                .getDriver()
                .findElement(By.xpath("//h2//a[contains(text(), '%s')]".formatted(itemName)))
                .click();
        return new DigitalDownloadsProductPage();
    }
}
