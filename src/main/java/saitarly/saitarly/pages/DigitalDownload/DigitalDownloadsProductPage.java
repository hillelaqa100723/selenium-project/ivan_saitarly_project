package saitarly.saitarly.pages.DigitalDownload;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import saitarly.saitarly.pages.content.BaseContentPage;
import saitarly.saitarly.utils.MyFileUtils;

import java.io.File;

@Slf4j
public class DigitalDownloadsProductPage extends BaseContentPage {

    @FindBy(css = ".download-sample-button")
    public WebElement downloadSampleButton;

    public DigitalDownloadsProductPage clickDownloadSampleButton(){
        downloadSampleButton.click();
        return this;
    }
    @SneakyThrows
    public File downloadSample(String fileName){
        log.info("Downloading a sample file with the name: {}", fileName);
        File downloadFolder = MyFileUtils.getDownloadsFolder();
        File downloadedFile = new File(downloadFolder, fileName);
        if (downloadedFile.exists()){
            downloadedFile.delete();
        }
        clickDownloadSampleButton();
        return MyFileUtils.waitTillFileIsDownloaded(downloadedFile);
    }
}
