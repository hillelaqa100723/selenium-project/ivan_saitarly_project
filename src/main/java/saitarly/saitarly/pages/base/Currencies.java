package saitarly.saitarly.pages.base;

import lombok.Getter;

@Getter
public enum Currencies {
    US_DOLLAR("US Dollar"),
    EURO("Euro");

    private final String value;


    Currencies(String value) {
        this.value = value;
    }

}
