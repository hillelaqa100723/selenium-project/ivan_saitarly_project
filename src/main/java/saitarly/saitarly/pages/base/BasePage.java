package saitarly.saitarly.pages.base;

import io.qameta.allure.Step;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import saitarly.saitarly.drivers.WebDriverHolder;
import saitarly.saitarly.pages.base.main_menu.MainMenu;
import saitarly.saitarly.pages.base.navigate_menu.NavigateMenu;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

@Slf4j
public class BasePage {
    @FindBy(id = "customerCurrency")
    private WebElement selectCurrency;

    @FindBy(xpath = "//span[@class='cart-qty']")
    private static WebElement cartQuantity;

    @FindBy(css = ".bar-notification.success")
    private WebElement successMessage;


    public BasePage() {
        PageFactory.initElements(WebDriverHolder.getInstance().getDriver(), this);
    }

    @Step
    public static MainMenu getMainMenu() {
        log.info("Main Menu is visible");
        return new MainMenu();
    }

    @Step
    public NavigateMenu getNavigationMenu() {
        log.info("Navigation menu is visible");
        return new NavigateMenu();
    }
    @Step
    public BasePage selectCurrency(Currencies currency) {
        log.info("Currency is selected");
        new Select(selectCurrency).selectByVisibleText(currency.getValue());
        return new BasePage();
    }

    @SneakyThrows
    protected void sleep(long ms) {
        Thread.sleep(ms);
    }

    @FindBy(xpath = "//h2[contains(text(),'Welcome to our store')]")
    private WebElement MainPage;

    public boolean isTheMainPageLoaded() {
        return MainPage.isDisplayed();
    }

    @Step
    public int getShoppingCartValue() {
        log.info("Get number of current product in the shopping cart");
        String cartValueText = cartQuantity.getText();
        String numericPart = cartValueText.replaceAll("\\D", "");

        try {
            return Integer.parseInt(numericPart);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public static boolean verifyAllProductPricesInCurrency(String currencyCode) {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();

        List<WebElement> productPrices = driver.findElements(By.xpath("//span[@class='price actual-price']"));

        if (productPrices.isEmpty()) {
            System.out.println("No product prices found on the page.");
            return false;
        }

        for (WebElement productPrice : productPrices) {
            if (!productPrice.getText().contains(currencyCode)) {
                System.out.println("Product price is not in the expected currency: " + productPrice.getText());
                return false;
            }
        }
        return true;
    }


    public BasePage waitForSuccessMessage(){
        log.info("Waiting for success message");
        WebDriverHolder.getInstance()
                .getWait()
                .until(ExpectedConditions.visibilityOf(successMessage));
        return this;
    }

    public BasePage closeSuccessMessage(){
        log.info("Closing success message");
        successMessage.findElement(By.cssSelector(".close")).click();
        return this;
    }
}

