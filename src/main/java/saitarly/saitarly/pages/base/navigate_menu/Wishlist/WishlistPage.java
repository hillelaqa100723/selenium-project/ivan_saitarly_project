package saitarly.saitarly.pages.base.navigate_menu.Wishlist;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import saitarly.saitarly.drivers.WebDriverHolder;
import saitarly.saitarly.model.ProductModelWishlist;
import saitarly.saitarly.pages.base.BasePage;

import java.util.LinkedList;
import java.util.List;


@Slf4j
public class WishlistPage extends BasePage {

    @FindBy(css = "table.cart")
    private WebElement itemsTable;

    @FindBy(xpath = "//button[@name='addtocartbutton']")
    public WebElement addToCardButton;

    public List<ProductModelWishlist> getProductsFromTable() {
        List<ProductModelWishlist> list = new LinkedList<>();

        for (WebElement row : itemsTable.findElements(By.cssSelector("tbody tr"))) {
            ProductModelWishlist productModelWishlist = new ProductModelWishlist();
            String name = row.findElement(By.cssSelector(".product-name")).getText().trim();
            String priceAsText = row.findElement(By.cssSelector(".product-unit-price"))
                    .getText()
                    .substring(1)
                    .replaceAll(",", "");
            String qtyAsString = row.findElement(By.cssSelector(".qty-input")).getAttribute("value");

            productModelWishlist.productName(name);
            productModelWishlist.price(Double.parseDouble(priceAsText));
            productModelWishlist.qty(Integer.parseInt(qtyAsString));
            list.add(productModelWishlist);
        }
        log.info("Retrieved wishlist items from the table: {}", list);
        return list;
    }


    public static List<WebElement> getWishlistItems() {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        return driver.findElements(By.xpath("//tbody/tr"));
    }


    public WishlistPage useCheckboxAddToCart(String itemName) {
        for (WebElement row : itemsTable.findElements(By.cssSelector("tbody tr"))) {
            if (row.findElement(By.cssSelector(".product-name")).getText().equals(itemName)) {
                row.findElement(By.xpath(".//input[@name='addtocart']")).click();
                log.info("Clicked 'Add to Cart' checkbox for item: {}", itemName);
                return new WishlistPage();
            }
        }
        return new WishlistPage();
    }
    public void clickOnAddToCartButton() {
        log.info("Clicked 'Add to Cart' button.");
        addToCardButton.click();
    }
}
