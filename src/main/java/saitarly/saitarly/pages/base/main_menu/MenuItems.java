package saitarly.saitarly.pages.base.main_menu;

import lombok.Getter;

@Getter
public enum MenuItems {
    LOG_IN("login"),
    LOG_OUT("logout"),
    WISHLIST("wishlist"),
    MY_ACCOUNT("account"),
    SHOPPING_CART("cart"),
    REGISTER("register");

    private final String value;

    MenuItems(String value) {
        this.value = value;
    }

}
