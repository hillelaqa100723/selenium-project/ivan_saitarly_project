package saitarly.saitarly.pages.base.main_menu;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import saitarly.saitarly.drivers.WebDriverHolder;
import saitarly.saitarly.pages.base.BasePage;
import saitarly.saitarly.pages.base.navigate_menu.ShoppingCartPage.CartPage;
import saitarly.saitarly.pages.base.navigate_menu.Wishlist.WishlistPage;
import saitarly.saitarly.pages.login.LoginPage;
import saitarly.saitarly.pages.my_account.MyAccountPage;

@Slf4j
public class MainMenu {
    private void selectMenuItem(MenuItems menuItem) {
        log.info("Selecting {} menu item", menuItem);
        findMenuItem(menuItem).click();
    }

    public LoginPage selectLogin() {
        log.info("Selecting Login menu item");
        selectMenuItem(MenuItems.LOG_IN);
        return new LoginPage();
    }

    public MyAccountPage selectMyAccount() {
        log.info("Selecting My Account menu item");
        selectMenuItem(MenuItems.MY_ACCOUNT);
        return new MyAccountPage();
    }

    public BasePage selectLogout() {
        log.info("Selecting Logout menu item");
        selectMenuItem(MenuItems.LOG_OUT);
        return new BasePage();
    }

    public Boolean isLogOutMenuItemVisible() {
        log.info("Checking if Logout menu item is visible");
        return findMenuItem(MenuItems.LOG_OUT)
                .isDisplayed();
    }

    public Boolean isMyAccountMenuItemVisible() {
        log.info("Checking if My Account menu item is visible");
        return findMenuItem(MenuItems.MY_ACCOUNT)
                .isDisplayed();
    }

    public WishlistPage selectWishlist() {
        log.info("Selecting Wishlist menu item");
        selectMenuItem(MenuItems.WISHLIST);
        return new WishlistPage();
    }

    public Boolean isRegisterMenuItemVisible() {
        log.info("Checking if Register menu item is visible");
        return findMenuItem(MenuItems.REGISTER).isDisplayed();
    }

    public Boolean isLoginMenuItemVisible() {
        log.info("Checking if Login menu item is visible");
        return findMenuItem(MenuItems.LOG_IN).isDisplayed();
    }


    private WebElement findMenuItem(MenuItems menuItem) {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        return driver
                .findElement(By.cssSelector(".ico-%s".formatted(menuItem.getValue())));

    }

    @SneakyThrows
    public CartPage selectShoppingCart() {
        log.info("Selecting Shopping Cart menu item");
        selectMenuItem(MenuItems.SHOPPING_CART);
        WebDriverHolder.getInstance().getWait()
                .until(ExpectedConditions.urlContains("/cart"));
        Thread.sleep(2000);
        return new CartPage();
    }
}
