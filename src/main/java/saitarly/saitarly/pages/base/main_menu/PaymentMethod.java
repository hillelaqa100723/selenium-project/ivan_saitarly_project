package saitarly.saitarly.pages.base.main_menu;

public enum PaymentMethod {
    CREDIT_CARD,
    CHECK_MONEY_ORDER
}
