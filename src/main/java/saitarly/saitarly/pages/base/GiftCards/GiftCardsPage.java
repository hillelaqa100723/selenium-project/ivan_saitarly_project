package saitarly.saitarly.pages.base.GiftCards;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import saitarly.saitarly.drivers.WebDriverHolder;
import saitarly.saitarly.pages.content.BaseContentPage;

@Slf4j
public class GiftCardsPage extends BaseContentPage {

    public void clickProductByName(String productName) {
        By productLocator = By.linkText("$50 Physical Gift Card");
        WebElement product = findElementWithLocator(productLocator);
        product.click();
    }

    private WebElement findElementWithLocator(By locator) {
        log.info("Found element with locator: {}", locator);
        return WebDriverHolder.getInstance().getDriver().findElement(locator);
    }


}
