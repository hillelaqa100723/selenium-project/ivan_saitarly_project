package saitarly.saitarly.pages.base.GiftCards;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import saitarly.saitarly.pages.base.BasePage;


@Slf4j
public class GiftCardProductPage extends BasePage {


    @FindBy(css = ".add-to-cart-button")
    public WebElement addToCartButton;

    @FindBy(id = "bar-notification")
    private static WebElement errorMessage;

    @FindBy(xpath = "//div[@id='bar-notification']//p")
    public static WebElement errorMessageText;

    @FindBy(css = ".recipient-name")
    public static WebElement recipientNameField;

    @FindBy(css = ".sender-name")
    public static WebElement senderNameField;

    public static void addSenderName(String senderName){
        log.info("Entering the sender's name: {}", senderName);
        senderNameField.clear();
        senderNameField.sendKeys(senderName);
    }

    public static void addRecipientName (String recipientName){
        log.info("Entering the recipient's name: {}", recipientName);
        recipientNameField.clear();
        recipientNameField.sendKeys(recipientName);
    }
    public static String getErrorMessageText() {
        String message = errorMessageText.getText().trim();
        log.info("Error message received: {}", message);
        return message;
    }

    public static boolean isErrorMessageDisplayed() {
        return errorMessage.isDisplayed();
    }

    public void clickOnAddToCartButton() {
        addToCartButton.click();
    }


}
