package saitarly.saitarly.pages.base.checkout;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import saitarly.saitarly.model.PaymentCardModel;

@Slf4j
public class CheckoutPaymentInformation extends CheckoutProcessBasePage {
    @FindBy(id = "CreditCardType")
    private WebElement creditCardTypeDropdown;

    @FindBy(id = "CardholderName")
    private WebElement cardholderName;
    @FindBy(id = "CardNumber")
    private WebElement cardNumber;

    @FindBy(id = "ExpireMonth")
    private WebElement expireMonthDropdown;

    @FindBy(id = "ExpireYear")
    private WebElement expireYearDropdown;

    @FindBy(id = "CardCode")
    private WebElement cardCode;

    public CheckoutPaymentInformation() {
        super();
        saveButtonClass = ".payment-info-next-step-button";
    }

    public CheckoutPaymentInformation setCardInformation(PaymentCardModel cardInformation){
        new Select(creditCardTypeDropdown).selectByValue(cardInformation.getPaymentMethod().getValue());
        cardholderName.clear();
        cardholderName.sendKeys(cardInformation.getCardHolderName());
        cardNumber.clear();
        cardNumber.sendKeys(cardInformation.getCardNumber());
        new Select(expireMonthDropdown).selectByValue(cardInformation.getExpMonth());
        new Select(expireYearDropdown).selectByVisibleText(cardInformation.getExpYear());
        cardCode.clear();
        cardCode.sendKeys(cardInformation.getCvv());
        log.info("Card information for payment has been set.");
        return this;
    }
}
