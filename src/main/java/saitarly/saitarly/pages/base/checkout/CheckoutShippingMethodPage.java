package saitarly.saitarly.pages.base.checkout;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import saitarly.saitarly.drivers.WebDriverHolder;


@Slf4j
public class CheckoutShippingMethodPage extends CheckoutProcessBasePage {

    public CheckoutShippingMethodPage() {
        super();
        saveButtonClass = ".shipping-method-next-step-button";
    }

    public CheckoutShippingMethodPage selectShippingMethod(ShippingMethod shippingMethod) {
        WebDriverHolder.getInstance()
                .getDriver()
                .findElement(By.id("shippingoption_" + shippingMethod.ordinal()))
                .click();
        log.info("Shipping method selected:" + shippingMethod.name());
        return this;
    }
}
