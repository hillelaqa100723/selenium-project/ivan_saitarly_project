package saitarly.saitarly.pages.base.checkout;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import saitarly.saitarly.drivers.WebDriverHolder;
import saitarly.saitarly.pages.base.main_menu.PaymentMethod;


@Slf4j
public class CheckoutPaymentMethodPage extends CheckoutProcessBasePage {

    public CheckoutPaymentMethodPage() {
        super();
        saveButtonClass = ".payment-method-next-step-button";
    }

    public CheckoutPaymentMethodPage selectPaymentMethod(PaymentMethod paymentMethod) {
        WebDriverHolder.getInstance()
                .getDriver()
                .findElement(By.id("paymentmethod_" + paymentMethod.ordinal()))
                .click();
        log.info("Payment method selected: " + paymentMethod.name());
        return this;
    }
}