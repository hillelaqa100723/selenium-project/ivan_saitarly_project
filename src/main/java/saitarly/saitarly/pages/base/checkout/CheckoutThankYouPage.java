package saitarly.saitarly.pages.base.checkout;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import saitarly.saitarly.drivers.WebDriverHolder;
import saitarly.saitarly.utils.MyFileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class CheckoutThankYouPage extends CheckoutProcessBasePage{

    public CheckoutThankYouPage() {
        super();
        saveButtonClass = ".order-completed-continue-button";
    }

    @FindBy(xpath = "//a[contains(text(),'Click here for order details.')]")
    private static WebElement orderDetailsButton;

    public static OrderInformationPage clickOnOrderDetailsButton() {
        orderDetailsButton.click();
        return new OrderInformationPage();
    }

    @FindBy(xpath = "//strong[contains(text(),'Your order has been successfully processed!')]")
    public static WebElement successfulMessage;

    public static String getSuccessfulMessage() {
        return successfulMessage.getText();
    }


    @FindBy(xpath = "//em//a")
    public static List<WebElement> products;


    public static List<String> getProducts() {
        List<String> list = new ArrayList<>();

        for (WebElement notebookNames : products) {
            list.add(notebookNames.getText());
        }
        return list;
    }

    @FindBy(css = ".order-total")
    private  WebElement orderTotal;

    public  double getOrderTotal() {
        String totalAmountText = orderTotal.getText();
        return Double.parseDouble(totalAmountText.replaceAll("[^\\d.]+", ""));
    }

    @FindBy(css = ".pdf-invoice-button")
    public WebElement pdfInvoiceButton;

    public CheckoutThankYouPage clickPdfInvoiceButton(){
        pdfInvoiceButton.click();
        return this;
    }
    @SneakyThrows
    public File selectPdfInvoice() {
        File downloadFolder = MyFileUtils.getDownloadsFolder();
        File downloadedFile = new File(downloadFolder, "order.pdf");
        if (downloadedFile.exists()) {
            downloadedFile.delete();
        }
        log.info("Click on the download PDF invoice button.");
        clickPdfInvoiceButton();

        WebDriverWait wait = WebDriverHolder.getInstance().getWait();

        File downloadedFileInFolder = wait.until(driver -> {
            File file = new File(downloadFolder, "order.pdf");
            if (file.exists()) {
                log.info("File 'order.pdf' successfully uploaded.");
                return file;
            }
            return null;
        });

        if (downloadedFileInFolder != null) {
            return downloadedFileInFolder;
        } else {
            log.error("File 'order.pdf' was not downloaded.");
            throw new Exception("File 'order.pdf' was not downloaded.");
        }
    }

}
