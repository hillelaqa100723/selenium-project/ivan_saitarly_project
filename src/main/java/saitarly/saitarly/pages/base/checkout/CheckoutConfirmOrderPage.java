package saitarly.saitarly.pages.base.checkout;

public class CheckoutConfirmOrderPage extends CheckoutProcessBasePage{

    public CheckoutConfirmOrderPage() {
        super();
        saveButtonClass = ".confirm-order-next-step-button";
    }
}
