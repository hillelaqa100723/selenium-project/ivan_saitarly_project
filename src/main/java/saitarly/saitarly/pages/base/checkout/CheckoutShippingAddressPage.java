package saitarly.saitarly.pages.base.checkout;

public class CheckoutShippingAddressPage extends CheckoutProcessBasePage{
    public CheckoutShippingAddressPage() {
        super();
        saveButtonClass = ".new-address-next-step-button";
    }
}