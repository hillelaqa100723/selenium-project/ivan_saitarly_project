package saitarly.saitarly.pages.base.checkout;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import saitarly.saitarly.drivers.WebDriverHolder;
import saitarly.saitarly.pages.base.BasePage;



@Slf4j
public class CheckoutProcessBasePage extends BasePage {

    protected String saveButtonClass;

    public CheckoutProcessBasePage() {
        super();
        sleep(3000);
    }

    @SneakyThrows
    public <T> T save(Class<T> pageToReturn) {
        log.info("Pressing save button with class: {}", saveButtonClass);
        WebDriverHolder.getInstance()
                .getDriver()
                .findElement(By.cssSelector(saveButtonClass))
                .click();
        return pageToReturn.getConstructor().newInstance();
    }
}
