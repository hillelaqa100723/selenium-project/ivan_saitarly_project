package saitarly.saitarly.pages.base.checkout;

public enum PaymentMethod {
    CREDIT_CARD,
    CHECK_MONEY_ORDER
}
