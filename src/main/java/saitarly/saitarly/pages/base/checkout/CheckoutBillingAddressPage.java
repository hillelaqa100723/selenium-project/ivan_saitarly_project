package saitarly.saitarly.pages.base.checkout;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Slf4j
public class CheckoutBillingAddressPage extends CheckoutProcessBasePage {
    public CheckoutBillingAddressPage() {
        super();
        saveButtonClass = ".new-address-next-step-button";
    }

    @FindBy(id = "ShipToSameAddress")
    private WebElement shipToTheSameAddressCheckBox;

    public CheckoutBillingAddressPage shipToTheSameAddress() {
        if (!shipToTheSameAddressCheckBox.isSelected()) {
            shipToTheSameAddressCheckBox.click();
            log.info("Option checked 'Ship to the same address'.");
        }
        return this;
    }

    public CheckoutBillingAddressPage shipToAnotherAddress() {
        if (shipToTheSameAddressCheckBox.isSelected()) {
            shipToTheSameAddressCheckBox.click();
            log.info("Option removed 'Ship to the same address'.");
        }
        return this;
    }

}
