package saitarly.saitarly.pages.base.checkout;

public enum ShippingMethod {
    GROUND,
    NEXT_DAY_AIR,
    SECOND_DAY_AIR
}
