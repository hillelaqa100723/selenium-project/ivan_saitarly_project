package saitarly.saitarly.pages.base.footer.CompareProductsPage;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import saitarly.saitarly.pages.base.BasePage;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class CompareProductsPage extends BasePage {

    @FindBy(css = ".clear-list")
    public WebElement clearListButton;

    @FindBy(css = ".no-data")
    public WebElement noDataMessage;

    @FindBy(xpath = "//tbody/tr[@class='product-price']/td[@style]")
    private List<WebElement> compareListItems;


    public List<String> getCompareProductPrices() {
        log.info("Getting product prices from the comparison page.");
        return compareListItems.stream()
                .map(this::extractNumericValueFromPrice)
                .collect(Collectors.toList());
    }

    private String extractNumericValueFromPrice(WebElement item) {
        String priceText = item.getText().trim();
        double numericValue = Double.parseDouble(priceText.replaceAll("[^0-9.]", ""));
        return String.format("%.2f", numericValue);
    }

    public void clickOnClearListButton() {
        clearListButton.click();
    }

    public String getNoDataMessage() {
        return noDataMessage.getText().trim();
    }
}
