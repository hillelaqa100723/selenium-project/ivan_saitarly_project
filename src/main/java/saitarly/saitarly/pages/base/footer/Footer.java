package saitarly.saitarly.pages.base.footer;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import saitarly.saitarly.pages.base.BasePage;

public class Footer extends BasePage {

    @FindBy(xpath = "//a[contains(text(),'Compare products list')]")
    private WebElement CompareProductsList;

    public void clickOnProductsListButton(){
        CompareProductsList.click();
    }
}
