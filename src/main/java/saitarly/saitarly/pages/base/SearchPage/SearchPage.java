package saitarly.saitarly.pages.base.SearchPage;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import saitarly.saitarly.drivers.WebDriverHolder;
import saitarly.saitarly.pages.base.BasePage;

@Slf4j
public class SearchPage extends BasePage {

        @FindBy(id = "small-searchterms")
        private WebElement searchField;

        @FindBy(css = ".search-box-button")
        private WebElement searchButton;

        public void addSearchData(String searchData) {
                searchField.clear();
                searchField.sendKeys(searchData);
        }

        public void clickSearchButton() {
                searchButton.click();
        }


        public boolean withNoProducts() {
                WebElement emptyStateText = WebDriverHolder.getInstance().getDriver()
                        .findElement(By.xpath("//div[contains(text(),'No products were found that matched your criteria.')]"));

                if (emptyStateText.isDisplayed()) {
                        log.info("No products were found that matched the search criteria.");
                        return true;
                } else {
                        log.info("Products matching the search criteria are found.");
                        return false;
                }
        }

        public boolean isSearchResultsDisplayed(String name) {
                WebElement searchedElement = WebDriverHolder.getInstance().getDriver()
                        .findElement(By.xpath("//a[contains(text(),'" + name + "')]"));

                if (searchedElement.isDisplayed()) {
                        log.info("Search results for '{}' are displayed.", name);
                        return true;
                } else {
                        log.info("Search results for '{}' are not displayed.", name);
                        return false;
                }
        }
}


