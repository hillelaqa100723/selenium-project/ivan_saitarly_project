package saitarly.saitarly.pages.content.computers;


import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import saitarly.saitarly.drivers.WebDriverHolder;
import saitarly.saitarly.model.ProductModel;
import saitarly.saitarly.pages.content.BaseContentPage;

import java.util.List;

@Slf4j
public class NotebooksPage extends BaseContentPage {


    public List<ProductModel> getNotebooks() {
        log.info("Getting list of notebooks");
        return getProductAsList();
    }

    public ProductModel findProductByName(String productName) {
        log.info("Finding product by name: {}", productName);
        List<ProductModel> notebooks = getNotebooks();

        for (ProductModel product : notebooks) {
            if (product.productName().equals(productName)) {
                return product;
            }
        }
        return null;
    }

    public void clickProductByName(String productName) {
        log.info("Clicking on product by name: {}", productName);
        By productLocator = By.linkText("HP Envy 15.6-Inch Sleekbook");
        WebElement product = findElementWithLocator(productLocator);
        product.click();
    }

    private WebElement findElementWithLocator(By locator) {
        return WebDriverHolder.getInstance().getDriver().findElement(locator);
    }

    public NotebooksPage addNoteBookToCart(String noteBookName) {
        log.info("Adding notebook to cart: {}", noteBookName);
        addItemToCard(noteBookName);
        return new NotebooksPage();
    }

    public NotebooksPage addNoteBooksToCompareList(String noteBookName) {
        log.info("Adding notebook to compare list: {}", noteBookName);
        addItemToCompereList(noteBookName);
        return new NotebooksPage();
    }

    public boolean isProductQtyTrue(int expQty){
        log.info("Checking if product quantity is true: {}", expQty);
        return  getProductQty() == expQty;
    }

    public NotebooksPage addNoteBookToWishlist(String noteBookName) {
        log.info("Adding notebook to wishlist: {}", noteBookName);
        addItemToWishlist(noteBookName);
        return new NotebooksPage();
    }
}
