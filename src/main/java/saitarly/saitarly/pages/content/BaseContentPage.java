package saitarly.saitarly.pages.content;


import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import saitarly.saitarly.model.ProductModel;
import saitarly.saitarly.model.enums.SortByDirection;
import saitarly.saitarly.pages.base.BasePage;

import java.util.LinkedList;
import java.util.List;

@Slf4j
public class BaseContentPage extends BasePage {
    @FindBy(css = ".page-title h1")
    private WebElement contentPageTitle;

    @FindBy(id = "products-orderby")
    private WebElement sortByElement;

    @FindBy(id = "products-pagesize")
    private WebElement displaySizeElement;

    @FindBy(css = ".item-box .details")
    private static List<WebElement> productDetails;

    @FindBy(css = ".next-page")
    private  WebElement nextButton;

    @FindBy(css = ".previous-page")
    private WebElement previousButton;

    @FindBy(css = "ul li.current-page")
    private WebElement expPageNumber;

    public String getContentPageTitleText() {
        return contentPageTitle.getText();
    }

    public BaseContentPage selectSortBy(SortByDirection sortByDirection) {
        log.info("Selecting sort by: {}", sortByDirection);
        new Select(sortByElement)
                .selectByValue(sortByDirection.getValue());
        sleep(1500);
        return new BaseContentPage();
    }

    public BaseContentPage selectDisplayPerPage(int count) {
        log.info("Selecting display per page: {}", count);
        new Select(displaySizeElement)
                .selectByValue(String.valueOf(count));
        return new BaseContentPage();
    }

    public static List<ProductModel> getProductAsList() {
        log.info("Getting product list");
        List<ProductModel> list = new LinkedList<>();

        for (WebElement product : productDetails) {
            String name = product.findElement(By.cssSelector("h2>a")).getText();
            String priceAsString = product.findElement(By.cssSelector(".actual-price")).getText();
            double price = Double.parseDouble(
                    priceAsString
                            .substring(1)
                            .replaceAll(",", ""));
            ProductModel productModel = new ProductModel(name, price);
            list.add(productModel);
        }
        return list;
    }

    public List<WebElement> getProductDetails() {
        return productDetails;
    }

    @SneakyThrows
    protected BaseContentPage addItemToCard(String itemName) {
        log.info("Adding item to cart: {}", itemName);
        for (WebElement item : productDetails) {
            if (item.findElement(By.cssSelector(".product-title>a")).getText().equals(itemName)) {
                item.findElement(By.cssSelector(".add-info>.buttons>.product-box-add-to-cart-button")).click();
                return this;
            }
        }
        throw new Exception("We did not found item with name " + itemName);
    }


    @SneakyThrows
    protected BaseContentPage addItemToCompereList(String itemName) {
        for (WebElement item : productDetails) {
            if (item.findElement(By.cssSelector(".product-title>a")).getText().equals(itemName)) {
                item.findElement(By.cssSelector(".add-info>.buttons>.add-to-compare-list-button")).click();
                return this;
            }
        }
        throw new Exception("We did not found item with name " + itemName);
    }

    @SneakyThrows
    protected BaseContentPage addItemToWishlist(String itemName) {
        for (WebElement item : productDetails) {
            if (item.findElement(By.cssSelector(".product-title>a")).getText().equals(itemName)) {
                item.findElement(By.cssSelector(".add-info>.buttons>.add-to-wishlist-button")).click();
                return this;
            }
        }
        throw new Exception("We did not found item with name " + itemName);
    }


    public static int getProductQty(){
        return productDetails.size();
    }

    public void clickNextPageButton(){
        nextButton.click();
    }

    public void clickPrevPageButton(){
        previousButton.click();
    }

    public int getExpPageNumber(){
        log.info("Getting expected page number");
    String pageNumberText = expPageNumber.getText();
        return Integer.parseInt(pageNumberText);
    }
}





