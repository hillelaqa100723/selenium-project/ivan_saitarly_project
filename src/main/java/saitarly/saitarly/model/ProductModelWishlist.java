package saitarly.saitarly.model;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
@EqualsAndHashCode(callSuper = true)
public class ProductModelWishlist extends ProductModel{
    private int qty;
}
