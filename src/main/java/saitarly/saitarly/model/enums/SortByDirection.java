package saitarly.saitarly.model.enums;

import lombok.Getter;

@Getter
public enum SortByDirection {
    NAME_A_Z("5"),
    NAME_Z_A("6"),
    PRICE_LOW_TO_HIGH("10"),
    PRICE_HIGH_TO_LOW("11");

    private final String value;

    SortByDirection(String value) {
        this.value = value;
    }

}
