package saitarly.saitarly.model.enums;

import lombok.Getter;


@Getter
public enum CreditCardType {
    VISA("visa"),
    MASTERCARD("MasterCard"),
    DISCOVER("Discover"),
    AMEX("Amex");

    private final String value;

    CreditCardType(String value) {
        this.value = value;
    }
}