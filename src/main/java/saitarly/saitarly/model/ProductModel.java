package saitarly.saitarly.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import saitarly.saitarly.model.enums.SortByDirection;

import java.util.Comparator;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(fluent = true)
public class ProductModel {
    private String productName;
    private Double price;


    public static Comparator<ProductModel> getComparator(SortByDirection sortByDirection) {
        switch (sortByDirection) {
            case NAME_A_Z -> {
                return new Comparator<ProductModel>() {
                    @Override
                    public int compare(ProductModel o1, ProductModel o2) {
                        return o1.productName().compareTo(o2.productName());
                    }
                };
            }
            case NAME_Z_A -> {
                return new Comparator<ProductModel>() {
                    @Override
                    public int compare(ProductModel o1, ProductModel o2) {
                        return o2.productName().compareTo(o1.productName());
                    }
                };
            }
            case PRICE_LOW_TO_HIGH -> {
                return new Comparator<ProductModel>() {
                    @Override
                    public int compare(ProductModel o1, ProductModel o2) {
                        return o1.price().compareTo(o2.price());
                    }
                };
            }
            case PRICE_HIGH_TO_LOW -> {
                return new Comparator<ProductModel>() {
                    @Override
                    public int compare(ProductModel o1, ProductModel o2) {
                        return o2.price().compareTo(o1.price());
                    }
                };
            }
        }
        return null;
    }

}
