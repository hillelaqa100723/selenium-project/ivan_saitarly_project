package saitarly.saitarly.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import saitarly.saitarly.model.enums.CreditCardType;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class PaymentCardModel {
    private CreditCardType paymentMethod;
    private String cardHolderName;
    private String cardNumber;
    private LocalDate expirationDate;
    private String cvv;

    public String getExpMonth(){
        return String.valueOf(expirationDate.getMonth().getValue());
    }

    public String getExpYear(){
        return String.valueOf(expirationDate.getYear());
    }
}
