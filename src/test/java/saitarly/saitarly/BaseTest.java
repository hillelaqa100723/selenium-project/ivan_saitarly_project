package saitarly.saitarly;

import com.aventstack.extentreports.testng.listener.ExtentITestListenerAdapter;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import saitarly.saitarly.drivers.WebDriverHolder;
import saitarly.saitarly.tests.listeners.MyTestsListeners;
import saitarly.saitarly.utils.MyFileUtils;
import saitarly.saitarly.utils.PropertyReader;
@Slf4j
@Listeners({MyTestsListeners.class, ExtentITestListenerAdapter.class})

public class BaseTest {
    @BeforeSuite
    public void beforeClass() {
        MyFileUtils.prepareScreenshotsFolder();
        MyFileUtils.prepareDownloadsFolder();
        WebDriverHolder.getInstance();
    }

    @AfterSuite(alwaysRun = true)
    public void afterClass() {
        WebDriverHolder.getInstance().quitDriver();
    }

    protected void openUrl(String url) {
        String baseUrl = PropertyReader.getInstance().getProperty("baseUrl");
        if (url.startsWith("/")) {
            log.info("Open {} url", url);
            WebDriverHolder.getInstance().getDriver().get(baseUrl + url);
        }else{
            log.info("Open {} url", url);
            WebDriverHolder.getInstance().getDriver().get(url);
        }
    }


    protected void openUrl() {
        openUrl(PropertyReader.getInstance().getProperty("baseUrl"));
    }

}
