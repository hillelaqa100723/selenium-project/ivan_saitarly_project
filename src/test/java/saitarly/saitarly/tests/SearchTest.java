package saitarly.saitarly.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import saitarly.saitarly.BaseTest;
import saitarly.saitarly.pages.base.BasePage;
import saitarly.saitarly.pages.base.SearchPage.SearchPage;

public class SearchTest extends BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    private static boolean loggedIn = false;

    @BeforeClass
    public void loginOnce() {
        if (!loggedIn) {
            try {
                new BasePage();
                BasePage
                        .getMainMenu()
                        .selectLogin()
                        .loginWithDefaultUser();
                loggedIn = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @DataProvider(name = "searchData")
    public Object[][] dataProvider() {
        return new Object[][]{
                {"Custom T-Shirt"},
                {"HTC smartphone"},
                {"First Prize Pies"},
                {"Vintage Style Engagement Ring"}
        };
    }
    @Test(dataProvider = "searchData")
    public void SearchTest(String data) {
        new SearchPage().addSearchData(data);
        new SearchPage().clickSearchButton();
        Assert.assertTrue(new SearchPage().isSearchResultsDisplayed(data));
    }
}
