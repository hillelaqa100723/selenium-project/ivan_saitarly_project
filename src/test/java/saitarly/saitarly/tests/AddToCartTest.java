package saitarly.saitarly.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import saitarly.saitarly.BaseTest;
import saitarly.saitarly.pages.base.BasePage;
import saitarly.saitarly.pages.content.computers.NotebooksPage;
import saitarly.saitarly.pages.content.computers.ProductPage;


import static org.assertj.core.api.Assertions.assertThat;

public class AddToCartTest extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void AddToCard() throws InterruptedException {
        new BasePage();
        BasePage
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers", "Notebooks");

        new NotebooksPage()
                .clickProductByName("HP Envy 15.6-Inch Sleekbook");

        Thread.sleep(2000);

        assertThat(new ProductPage().isProductPageLoaded())
                .as("Product Page is loaded.")
                .isTrue();

        int initialCartValue = new BasePage().getShoppingCartValue();

        new ProductPage().clickOnAddToCartButton();
        Thread.sleep(2000);

        assertThat(new ProductPage().isSuccessMessageDisplayed())
                .as("Success message is displayed")
                .isTrue();

        int finalCartValue = new BasePage().getShoppingCartValue();
        int expectedCartValue = initialCartValue + 1;
        assertThat(finalCartValue).as("Cart value increased by 1").isEqualTo(expectedCartValue);
    }
}
