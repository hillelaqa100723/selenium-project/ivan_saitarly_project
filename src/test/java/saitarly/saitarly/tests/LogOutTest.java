package saitarly.saitarly.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import saitarly.saitarly.BaseTest;
import saitarly.saitarly.pages.base.BasePage;

import static org.assertj.core.api.Assertions.assertThat;

public class LogOutTest extends BaseTest {
    @BeforeMethod
    public void beforeMethod(){
        openUrl();
    }
    @Test
    public void LogOut() {
        new BasePage();
        BasePage
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser();

        new BasePage();
        BasePage
            .getMainMenu()
                .selectLogout();

        assertThat(BasePage.getMainMenu().isRegisterMenuItemVisible())
                .as("Register menu item should be visible")
                .isTrue();
        assertThat(BasePage.getMainMenu().isLoginMenuItemVisible())
                .as("Login menu item should be visible")
                .isTrue();
    }
}
