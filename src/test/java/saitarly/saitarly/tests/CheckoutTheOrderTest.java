package saitarly.saitarly.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import saitarly.saitarly.BaseTest;
import saitarly.saitarly.drivers.WebDriverHolder;
import saitarly.saitarly.model.PaymentCardModel;
import saitarly.saitarly.model.enums.CreditCardType;
import saitarly.saitarly.pages.base.BasePage;
import saitarly.saitarly.pages.base.checkout.*;
import saitarly.saitarly.pages.base.main_menu.PaymentMethod;
import saitarly.saitarly.pages.content.computers.NotebooksPage;

import java.io.File;
import java.time.LocalDate;
import java.util.List;


import static org.assertj.core.api.Assertions.assertThat;

public class CheckoutTheOrderTest extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void CheckoutTheOrder() throws InterruptedException {
        new BasePage();
        BasePage
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers", "Notebooks");

        String note1 = "Asus Laptop";
        String note2 = "HP Envy 15.6-Inch Sleekbook";

        new NotebooksPage()
                .addNoteBookToCart(note1)
                .addNoteBookToCart(note2);
        Thread.sleep(1000);

        WebDriverHolder.ScrollHelper.scrollToTop();

        PaymentCardModel paymentCardModel = new PaymentCardModel()
                .setPaymentMethod(CreditCardType.MASTERCARD)
                .setCardHolderName("Some Person")
                .setCardNumber("2222400010000008")
                .setExpirationDate(LocalDate.of(2030, 3, 20))
                .setCvv("737");

        BasePage
                .getMainMenu()
                .selectShoppingCart()
                .addAgreementCheckbox()
                .clickOnCheckoutButton()
                .shipToTheSameAddress()
                .save(CheckoutShippingMethodPage.class)
                .selectShippingMethod(ShippingMethod.GROUND)
                .save(CheckoutPaymentMethodPage.class)
                .selectPaymentMethod(PaymentMethod.CREDIT_CARD)
                .save(CheckoutPaymentInformation.class)
                .setCardInformation(paymentCardModel)
                .save(CheckoutConfirmOrderPage.class)
                .save(CheckoutThankYouPage.class);

        assertThat(CheckoutThankYouPage.getSuccessfulMessage())
                .as("Message text should be 'Your order has been successfully processed!'")
                .isEqualTo("Your order has been successfully processed!");

        CheckoutThankYouPage.clickOnOrderDetailsButton();

        List<String> expectedProducts = List.of("Asus Laptop","HP Envy 15.6-Inch Sleekbook");
        List<String> actualProducts = CheckoutThankYouPage.getProducts();

        assertThat(actualProducts)
                .as("Actual products should match expected products")
                .containsExactlyInAnyOrderElementsOf(expectedProducts);

        double expectedOrderTotal = 2960.00; // Я хотел бы, в идеале, взять сумму из корзины и потом сравнить с суммой на CheckoutThankYouPage, но я это реализую уже на своем проэкте!
        double actualOrderTotal = new CheckoutThankYouPage().getOrderTotal();

        assertThat(actualOrderTotal)
                .as("Order Total should match the expected value")
                .isEqualTo(expectedOrderTotal);

        String expectedFileName = "order.pdf";

        File file = new CheckoutThankYouPage().selectPdfInvoice();

        assertThat(file)
                .hasFileName(expectedFileName);
    }
}
