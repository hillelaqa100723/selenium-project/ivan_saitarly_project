package saitarly.saitarly.tests;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import saitarly.saitarly.BaseTest;
import saitarly.saitarly.drivers.WebDriverHolder;
import saitarly.saitarly.model.ProductModelCart;
import saitarly.saitarly.pages.base.BasePage;
import saitarly.saitarly.pages.base.navigate_menu.ShoppingCartPage.CartPage;
import saitarly.saitarly.pages.content.computers.NotebooksPage;
import saitarly.saitarly.pages.login.LoginPage;
import saitarly.saitarly.utils.PropertyReader;

import static org.assertj.core.api.Assertions.assertThat;

public class DeleteFromCartTest extends BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void DeleteFromCart() throws InterruptedException {

        String userEmail = PropertyReader.getInstance().getProperty("defaultUser");
        String userPass = PropertyReader.getInstance().getProperty("userPass");

        new BasePage();
        BasePage basePage = BasePage.getMainMenu().selectLogin();

        assertThat(new LoginPage().isTheLoginPageLoaded())
                .as("Login form on the login page is loaded!")
                .isTrue();

        LoginPage.login(userEmail,userPass);

        Thread.sleep(2000);

        assertThat(BasePage.getMainMenu().isMyAccountMenuItemVisible())
                .as("MyAccount menu item should be visible")
                .isTrue();
        assertThat(BasePage.getMainMenu().isLogOutMenuItemVisible())
                .as("Logout menu item should be visible")
                .isTrue();

        basePage
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers", "Notebooks");


        String[] products = {"HP Envy 15.6-Inch Sleekbook", "Asus Laptop"};

        int expectedCartQuantity = 2;

        for (String product : products) {
            new NotebooksPage()
                    .addNoteBookToCart(product)
                    .waitForSuccessMessage()
                    .closeSuccessMessage();
        }

        JavascriptExecutor js = (JavascriptExecutor) WebDriverHolder.getInstance().getDriver();
        js.executeScript("window.scrollTo(0, 0)");


        new BasePage();
        BasePage cartPage = BasePage
                .getMainMenu()
                .selectShoppingCart();

        int actualCartQuantity = cartPage.getShoppingCartValue();

        assertThat(actualCartQuantity)
                .as("The actual number of items in the cart should match the expected quantity")
                .isEqualTo(expectedCartQuantity);


        ProductModelCart productInfo = new CartPage()
                .getProductInfo("Asus Laptop");

        assertThat(productInfo.productName())
                .isEqualTo(products[1]); // Эту проверку я сделал специально, так как будет нужна мне на работе

        new CartPage().removeItem(products[1]);

        int expectedCartQtyAfterRemove = 1;
        int actualCartQtyAfterRemove = cartPage.getShoppingCartValue();
        assertThat(actualCartQtyAfterRemove)
                .as("The actual number of items in the cart should match the expected quantity")
                .isEqualTo(expectedCartQtyAfterRemove);
    }
}




