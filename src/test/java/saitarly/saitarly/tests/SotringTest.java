package saitarly.saitarly.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import saitarly.saitarly.BaseTest;
import saitarly.saitarly.model.ProductModel;
import saitarly.saitarly.model.enums.SortByDirection;
import saitarly.saitarly.pages.base.BasePage;
import saitarly.saitarly.pages.content.computers.NotebooksPage;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class SotringTest extends BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        openUrl();
        BasePage
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");
    }


    @Test
    public void sortNoteBooks_Z_ATest() {
        List<ProductModel> notebooksBeforeSort = new NotebooksPage()
                .getNotebooks();

        notebooksBeforeSort.forEach(System.out::println);
        System.out.println();

        new NotebooksPage().selectSortBy(SortByDirection.NAME_Z_A);

        List<ProductModel> notebooksAfterSort = new NotebooksPage().getNotebooks();

        notebooksBeforeSort.sort(ProductModel.getComparator(SortByDirection.NAME_Z_A));

        notebooksBeforeSort.forEach(System.out::println);

        assertThat(notebooksAfterSort)
                .isEqualTo(notebooksBeforeSort);
    }

    @Test
    public void sortNoteBooks_A_ZTest() {

        List<ProductModel> notebooksBeforeSort = new NotebooksPage()
                .getNotebooks();

        notebooksBeforeSort.forEach(System.out::println);
        System.out.println();

        new NotebooksPage().selectSortBy(SortByDirection.NAME_A_Z);

        List<ProductModel> notebooksAfterSort = new NotebooksPage().getNotebooks();

        notebooksBeforeSort.sort(ProductModel.getComparator(SortByDirection.NAME_A_Z));

        notebooksBeforeSort.forEach(System.out::println);

        assertThat(notebooksAfterSort)
                .isEqualTo(notebooksBeforeSort);
    }



    @Test
    public void sortNoteBooksHigh_LowTest() {

        List<ProductModel> notebooksBeforeSort = new NotebooksPage()
                .getNotebooks();

        notebooksBeforeSort.forEach(System.out::println);
        System.out.println();

        new NotebooksPage().selectSortBy(SortByDirection.PRICE_HIGH_TO_LOW);

        List<ProductModel> notebooksAfterSort = new NotebooksPage().getNotebooks();

        notebooksBeforeSort.sort(ProductModel.getComparator(SortByDirection.PRICE_HIGH_TO_LOW));

        notebooksBeforeSort.forEach(System.out::println);

        assertThat(notebooksAfterSort)
                .asList()
                .isSorted()
                .isEqualTo(notebooksBeforeSort);
    }

    @Test
    public void sortNoteBooksLow_HighTest() {

        List<ProductModel> notebooksBeforeSort = new NotebooksPage()
                .getNotebooks();

        notebooksBeforeSort.forEach(System.out::println);
        System.out.println();

        new NotebooksPage().selectSortBy(SortByDirection.PRICE_LOW_TO_HIGH);

        List<ProductModel> notebooksAfterSort = new NotebooksPage().getNotebooks();

        notebooksBeforeSort.sort(ProductModel.getComparator(SortByDirection.PRICE_LOW_TO_HIGH));

        notebooksBeforeSort.forEach(System.out::println);

        assertThat(notebooksAfterSort)
                .asList()
                .isSorted()
                .isEqualTo(notebooksBeforeSort);
    }


}


