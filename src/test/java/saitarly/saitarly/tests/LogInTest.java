package saitarly.saitarly.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import saitarly.saitarly.BaseTest;
import saitarly.saitarly.pages.base.BasePage;
import saitarly.saitarly.pages.login.LoginPage;
import saitarly.saitarly.utils.PropertyReader;

import static org.assertj.core.api.Assertions.assertThat;

public class LogInTest extends BaseTest {
    @BeforeMethod
    public void beforeMethod(){
        openUrl();
        assertThat(new BasePage().isTheMainPageLoaded()).as("User should be on the main page").isTrue();
    }

    @Test
    public void loginTest() throws InterruptedException {
        String userEmail = PropertyReader.getInstance().getProperty("defaultUser");
        String userPass = PropertyReader.getInstance().getProperty("userPass");

        BasePage.getMainMenu().selectLogin();

        assertThat(new LoginPage().isTheLoginPageLoaded())
                .as("Login form on the login page is loaded!")
                .isTrue();

        LoginPage.login(userEmail,userPass);

        Thread.sleep(2000);

        assertThat(BasePage.getMainMenu().isMyAccountMenuItemVisible())
                .as("MyAccount menu item should be visible")
                .isTrue();
        assertThat(BasePage.getMainMenu().isLogOutMenuItemVisible())
                .as("Logout menu item should be visible")
                .isTrue();



        BasePage
                .getMainMenu()
                .selectLogout();
        assertThat(BasePage.getMainMenu().isRegisterMenuItemVisible())
                .as("Register menu item should be visible")
                .isTrue();
        assertThat(BasePage.getMainMenu().isLoginMenuItemVisible())
                .as("Login menu item should be visible")
                .isTrue();
    }
}
