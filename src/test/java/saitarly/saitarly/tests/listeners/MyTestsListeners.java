package saitarly.saitarly.tests.listeners;

import com.aventstack.extentreports.service.ExtentTestManager;
import com.aventstack.extentreports.testng.listener.ExtentITestListenerAdapter;
import io.qameta.allure.Attachment;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import saitarly.saitarly.drivers.WebDriverHolder;
import saitarly.saitarly.utils.MyFileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.Date;

@Slf4j
public class MyTestsListeners extends ExtentITestListenerAdapter{

    @Override
    public void onTestStart(ITestResult result) {
        super.onTestStart(result);
        log.info("Test {} started...", result.getName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        super.onTestSuccess(result);
        log.info("Test {} finished successfully!", result.getName());
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        super.onTestSkipped(result);
        log.info("Test {} skipped!", result.getName());
    }

    @Override
    public void onTestFailure(ITestResult result) {
        log.info("Test {} is FAILED!!!", result.getName());
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        File screenshotAs = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            File fileToSave = new File(MyFileUtils.getScreenshotsFolder(), "screenshots_" + result.getName() + "_" + new Date().getTime() + ".png");
            FileUtils.copyFile(screenshotAs, fileToSave);

            String encodedString = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(fileToSave));

            ExtentTestManager.getTest().addScreenCaptureFromBase64String(encodedString);
            getAttachment(fileToSave);
            log.info("Screenshot is saved to {}", fileToSave.getAbsolutePath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @SneakyThrows
    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] getAttachment(File file){
        return FileUtils.readFileToByteArray(file);
    }
}
