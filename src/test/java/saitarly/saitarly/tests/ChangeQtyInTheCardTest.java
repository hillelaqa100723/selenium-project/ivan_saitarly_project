package saitarly.saitarly.tests;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import saitarly.saitarly.BaseTest;
import saitarly.saitarly.drivers.WebDriverHolder;
import saitarly.saitarly.model.ProductModel;
import saitarly.saitarly.pages.base.BasePage;
import saitarly.saitarly.pages.base.navigate_menu.ShoppingCartPage.CartPage;
import saitarly.saitarly.pages.content.computers.NotebooksPage;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class ChangeQtyInTheCardTest extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void ChangeQtyInTheCard() throws InterruptedException {
        new NotebooksPage();

        BasePage.getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers", "Notebooks");

        String note1 = "Asus Laptop";
        String note2 = "HP Envy 15.6-Inch Sleekbook";

        new NotebooksPage()
                .addNoteBookToCart(note1)
                .addNoteBookToCart(note2);

        Thread.sleep(2000);

        JavascriptExecutor js = (JavascriptExecutor) WebDriverHolder.getInstance().getDriver();
        js.executeScript("window.scrollTo(0, 0)");

        CartPage cartPage = BasePage
                    .getMainMenu()
                    .selectShoppingCart();

        int actualCartQuantity = cartPage.getShoppingCartValue();

        List<String> expectedProducts = List.of("Asus Laptop", "HP Envy 15.6-Inch Sleekbook");
        List<String> actualProducts = cartPage.getProductsFromTable()
                    .stream()
                    .map(ProductModel::productName)
                    .collect(Collectors.toList());

        Assert.assertEquals(expectedProducts, actualProducts, "Product names are not as expected!");

        double price1 = cartPage.calcTotalProductPrice(note1);
        double price2 = cartPage.calcTotalProductPrice(note2);

        double totalCartPrice = cartPage.getTotalAmountFromCart();
        double expectedTotalPrice = price1 + price2;

        assertThat(totalCartPrice)
                    .as("The sum of the product prices does not correspond to the total amount of the cart")
                    .isEqualTo(expectedTotalPrice);

        new CartPage().clickOnQtyUpButton();

        int exCartQuantity = cartPage.getShoppingCartValue();
        assertThat(exCartQuantity)
                .as("Number of products on Shopping cart should be correct")
                .isEqualTo(actualCartQuantity + 1);

        double totalCartPriceAfterQtyIncrease = cartPage.getTotalAmountFromCart();
        double expectedTotalPriceAfterQtyIncrease = price1 + price1 + price2;

        assertThat(totalCartPriceAfterQtyIncrease)
                .as("The total product prices do not match the total cart amount before quantity increase")
                .isEqualTo(expectedTotalPriceAfterQtyIncrease);

            }
        }

