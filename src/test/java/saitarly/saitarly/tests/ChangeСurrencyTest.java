package saitarly.saitarly.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import saitarly.saitarly.BaseTest;
import saitarly.saitarly.pages.base.BasePage;
import saitarly.saitarly.pages.base.Currencies;

import static org.assertj.core.api.Assertions.assertThat;

public class ChangeСurrencyTest extends BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void changeCurrency() {
        BasePage
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");

        new BasePage().selectCurrency(Currencies.EURO);
        boolean areProductPricesInEuro = BasePage.verifyAllProductPricesInCurrency("€");

        assertThat(areProductPricesInEuro)
                .as("All product prices are in Euro currency")
                .isTrue();


        new BasePage().selectCurrency(Currencies.US_DOLLAR);
        boolean areProductPricesInDollar = BasePage.verifyAllProductPricesInCurrency("$");

        assertThat(areProductPricesInDollar)
                .as("All product prices are in Dollar currency")
                .isTrue();
    }

}
