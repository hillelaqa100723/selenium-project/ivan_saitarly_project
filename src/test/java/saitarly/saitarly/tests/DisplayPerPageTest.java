package saitarly.saitarly.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import saitarly.saitarly.BaseTest;
import saitarly.saitarly.pages.base.BasePage;
import saitarly.saitarly.pages.content.computers.NotebooksPage;

import static org.assertj.core.api.Assertions.assertThat;

public class DisplayPerPageTest extends BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void DisplayPerPage() throws InterruptedException {
        openUrl();
        BasePage
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");

        new NotebooksPage().selectDisplayPerPage(3);
        Thread.sleep(1000);

        assertThat(new NotebooksPage().isProductQtyTrue(3))
                .as("The product quantity on the page should be true for 3 products")
                .isTrue();

        new NotebooksPage().selectDisplayPerPage(6);
        Thread.sleep(1000);

        assertThat(new NotebooksPage().isProductQtyTrue(6))
                .as("The product quantity on the page should be true for 6 products")
                .isTrue();
    }
}
