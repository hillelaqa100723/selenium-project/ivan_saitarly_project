package saitarly.saitarly.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import saitarly.saitarly.BaseTest;
import saitarly.saitarly.pages.base.BasePage;
import saitarly.saitarly.pages.base.SearchPage.SearchPage;

public class SearchNonExistentProductTest extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
        BasePage
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser();
    }

    @Test
    public void SearchNonExistentProduct() throws InterruptedException {

        new SearchPage().addSearchData("ssdfsdfsdf");
        new SearchPage().clickSearchButton();
        Thread.sleep(1000);
        Assert.assertTrue(new SearchPage().withNoProducts());
    }
}