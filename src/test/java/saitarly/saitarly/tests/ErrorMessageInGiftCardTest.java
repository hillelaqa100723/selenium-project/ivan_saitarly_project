package saitarly.saitarly.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import saitarly.saitarly.BaseTest;
import saitarly.saitarly.pages.base.BasePage;
import saitarly.saitarly.pages.base.GiftCards.GiftCardProductPage;
import saitarly.saitarly.pages.base.GiftCards.GiftCardsPage;

import static org.assertj.core.api.Assertions.assertThat;

public class ErrorMessageInGiftCardTest extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void ErrorMessageInGiftCard() throws InterruptedException {
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItem("Gift Cards");

        int actualCartQuantity = new GiftCardProductPage().getShoppingCartValue();

        new GiftCardsPage().clickProductByName("$50 Physical Gift Card");


        new GiftCardProductPage().clickOnAddToCartButton();
        Thread.sleep(2000);

        assertThat(GiftCardProductPage.isErrorMessageDisplayed())
                .as("Error Message should be visible")
                .isTrue();
        assertThat(GiftCardProductPage.getErrorMessageText())
                .as("Error message is 'Enter valid recipient name' ")
                .isEqualTo("Enter valid recipient name");


        GiftCardProductPage.addRecipientName("test");
        GiftCardProductPage.addSenderName("");

        new GiftCardProductPage().clickOnAddToCartButton();
        Thread.sleep(2000);

        assertThat(GiftCardProductPage.isErrorMessageDisplayed())
                .as("Error Message should be visible")
                .isTrue();
        assertThat(GiftCardProductPage.getErrorMessageText())
                .as("Error message is 'Enter valid sender name' ")
                .isEqualTo("Enter valid sender name");

        GiftCardProductPage.addSenderName("Joe Biden");
        new GiftCardProductPage().clickOnAddToCartButton();
        Thread.sleep(2000);

        int exCartQuantity = new GiftCardsPage().getShoppingCartValue();
        assertThat(exCartQuantity)
                .as("Number of products on Shopping cart should be correct")
                .isEqualTo(actualCartQuantity + 1);

    }
}
