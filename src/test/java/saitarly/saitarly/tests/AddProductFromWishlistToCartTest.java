package saitarly.saitarly.tests;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import saitarly.saitarly.BaseTest;
import saitarly.saitarly.drivers.WebDriverHolder;
import saitarly.saitarly.model.ProductModel;
import saitarly.saitarly.pages.base.BasePage;
import saitarly.saitarly.pages.base.navigate_menu.Wishlist.WishlistPage;
import saitarly.saitarly.pages.content.computers.NotebooksPage;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class AddProductFromWishlistToCartTest extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        WebDriverHolder.getInstance().getDriver().manage().window().maximize();
        openUrl();
        BasePage
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser();
    }

    @Test
    public void AddProductFromWishlistToCart() throws InterruptedException {
        new BasePage()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers", "Notebooks");

        String note1 = "Asus Laptop";
        String note2 = "HP Envy 15.6-Inch Sleekbook";
        String note3 = "HP Spectre XT Pro UltraBook";
        String note4 = "Lenovo Thinkpad Carbon Laptop";

        int expectedCartQuantity = 2;

        new NotebooksPage()
                .addNoteBookToCart(note1)
                .addNoteBookToCart(note2);
        Thread.sleep(2000);

        int actualCartQuantity = new WishlistPage().getShoppingCartValue();

        assertThat(actualCartQuantity)
                .as("The actual number of items in the cart should match the expected quantity")
                .isEqualTo(expectedCartQuantity);

        new NotebooksPage()
                .addNoteBookToWishlist(note3)
                .addNoteBookToWishlist(note4);
        Thread.sleep(2000);

        JavascriptExecutor js = (JavascriptExecutor) WebDriverHolder.getInstance().getDriver();
        js.executeScript("window.scrollTo(0, 0)");

        new BasePage();
        BasePage
                .getMainMenu()
                .selectWishlist();

        List<String> expectedProducts = List.of("HP Spectre XT Pro UltraBook", "Lenovo Thinkpad Carbon Laptop");
        List<String> actualProducts = new WishlistPage().getProductsFromTable()
                .stream()
                .map(ProductModel::productName)
                .toList();

        assertThat(expectedProducts)
                .as("Added products are not equal to the products in the wishlist!")
                .isEqualTo(actualProducts);

        new WishlistPage().useCheckboxAddToCart(note4);
        new WishlistPage().clickOnAddToCartButton();

        int expectedCartQtyAfterAddToCart= 3;
        int actualCartQtyAfterAddToCart = new WishlistPage().getShoppingCartValue();

        assertThat(actualCartQtyAfterAddToCart)
                .as("The actual number of items in the cart should match the expected quantity")
                .isEqualTo(expectedCartQtyAfterAddToCart); // Тут добавил от себя проверки чтобы тест был более укомплектован
    }
}
