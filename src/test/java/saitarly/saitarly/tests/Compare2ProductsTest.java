package saitarly.saitarly.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import saitarly.saitarly.BaseTest;
import saitarly.saitarly.pages.base.BasePage;
import saitarly.saitarly.pages.base.footer.CompareProductsPage.CompareProductsPage;
import saitarly.saitarly.pages.base.footer.Footer;
import saitarly.saitarly.pages.content.computers.NotebooksPage;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class Compare2ProductsTest extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void CompareProductPrices() throws InterruptedException {
        new NotebooksPage();

        BasePage.getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers", "Notebooks");

        String[] products = {"Asus Laptop", "HP Envy 15.6-Inch Sleekbook"};

        for (String product : products) {
            new NotebooksPage()
                    .addNoteBooksToCompareList(product);
        }

        new Footer().clickOnProductsListButton();
        Thread.sleep(3000);

        CompareProductsPage compareProductsPage = new CompareProductsPage();

        List<String> expectedPrices = List.of("1500.00", "1460.00");

        List<String> actualPrices = compareProductsPage.getCompareProductPrices();

        for (String expectedPrice : expectedPrices) {
            System.out.println("Expected Price: " + expectedPrice);

            assertThat(actualPrices)
                    .as("Price " + expectedPrice + " is not found in the compared products")
                    .contains(expectedPrice);
        }

        compareProductsPage.clickOnClearListButton();
        Thread.sleep(2000);

        assertThat(compareProductsPage.getNoDataMessage())
                .as("No-data content text should be visible")
                .isEqualTo("You have no items to compare.");
    }


}
