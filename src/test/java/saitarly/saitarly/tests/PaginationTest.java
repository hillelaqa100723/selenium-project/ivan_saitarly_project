package saitarly.saitarly.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import saitarly.saitarly.BaseTest;
import saitarly.saitarly.model.ProductModel;
import saitarly.saitarly.pages.base.BasePage;
import saitarly.saitarly.pages.content.computers.NotebooksPage;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PaginationTest extends BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void PaginationTest() throws InterruptedException {
        openUrl();
        BasePage
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");

        new NotebooksPage().selectDisplayPerPage(3);
        Thread.sleep(2000);

        List<ProductModel> notebooksOnCurrentPage = new NotebooksPage().getNotebooks();

        new NotebooksPage().clickNextPageButton();
        Thread.sleep(2000);

        assertThat(new NotebooksPage().getExpPageNumber())
                .as("Page 2 should be highlighted")
                .isEqualTo(2);


        List<ProductModel> notebooksOnNextPage = new NotebooksPage().getNotebooks();

        assertThat(notebooksOnCurrentPage)
                .isNotEqualTo(notebooksOnNextPage);

        new NotebooksPage().clickPrevPageButton();
        Thread.sleep(2000);

        assertThat(new NotebooksPage().getExpPageNumber())
                .as("Page 1 should be highlighted")
                .isEqualTo(1);

        List<ProductModel> notebooksOnPrevPagePage = new NotebooksPage().getNotebooks();
        Thread.sleep(1000);

        assertThat(notebooksOnPrevPagePage)
                .isNotEqualTo(notebooksOnNextPage);
    }
}