package saitarly.saitarly.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import saitarly.saitarly.BaseTest;
import saitarly.saitarly.pages.DigitalDownload.DigitalDownloads;
import saitarly.saitarly.pages.base.BasePage;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;

public class DigitalDownloadsTest extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }
    @Test
    public void DigitalDownload() {
        BasePage
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItem("Digital downloads ");

        String expectedFileName = "Night_Vision_1.txt";

        File file = new DigitalDownloads()
                .selectProductItem("Night Visions")
                .downloadSample(expectedFileName);

        assertThat(file)
                .hasFileName(expectedFileName);
    }
}
